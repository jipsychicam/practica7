package com.example.user.basededatos;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by user on 15/01/2018.
 */

public class HelperBD extends SQLiteOpenHelper{

    public HelperBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table persona(id INTEGER PRIMARY KEY AUTOINCREMENT, cedula text, apellidos text, nombres text);");
    }
    public void insertar(String cedula, String apellidos, String nombres){
        ContentValues values = new ContentValues();
        values.put("cedula",cedula);
        values.put("apellidos",apellidos);
        values.put("nombres",nombres);
        this.getWritableDatabase().insert("persona", null,values);


    }
    public String leercedula (String cedula){
        String consulta ="";
        String []args = new String[] {cedula};
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM persona "+" WHERE cedula = ? ", args);
        if (cursor.moveToFirst()){
            do{
                String nombrePersona = cursor.getString(cursor.getColumnIndex("nombres"));
                String apellidosPersona = cursor.getString(cursor.getColumnIndex("apellidos"));
                String cedulaPersona = cursor.getString(cursor.getColumnIndex("cedula"));


                consulta += nombrePersona +" "+ apellidosPersona + " " + cedulaPersona +" \n" +" \n";


            }while (cursor.moveToNext());
        }
        return consulta;
    }


    public String eliminarcedula (String cedula){
        String eliminar = "";
        String []args = new String[] {cedula};
        Cursor cursor = this.getReadableDatabase().rawQuery("DELETE FROM persona " + " WHERE cedula =? ", args);
        if (cursor.moveToFirst()){

        }
        return eliminar;
    }



    public void eliminar(){

        this.getWritableDatabase().delete("persona", null, null);
    }



    public String leer(){
        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM persona", null);
        if (cursor.moveToFirst()){
            do{
                String nombrePersona = cursor.getString(cursor.getColumnIndex("nombres"));
                String apellidosPersona = cursor.getString(cursor.getColumnIndex("apellidos"));
                String cedulaPersona = cursor.getString(cursor.getColumnIndex("cedula"));
                consulta += nombrePersona +" "+ apellidosPersona + " " + cedulaPersona +" \n" +" \n";
            }while (cursor.moveToNext());
        }
        return consulta;
    }

    public void modificar(String cedula, String apellidos, String nombres){
        ContentValues values = new ContentValues();
        values.put("apellidos",apellidos);
        values.put("nombres",nombres);
        this.getWritableDatabase().update(
                "persona", values, "cedula='" + cedula +"'", null );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
