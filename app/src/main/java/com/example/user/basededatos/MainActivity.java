package com.example.user.basededatos;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button botonAgregar, botonModificar, botonEliminar, botonListar, botonEliminartodos, botonListartodos;
    EditText cajaCedula, cajaApellidos, cajaNombres;
    TextView datos;

    HelperBD personabd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cajaCedula = (EditText) findViewById(R.id.txtCedula);
        cajaApellidos = (EditText) findViewById(R.id.txtApellido);
        cajaNombres = (EditText) findViewById(R.id.txtNombre);
        botonAgregar = (Button) findViewById(R.id.btnAgregar);
        botonModificar = (Button) findViewById(R.id.btnModificar);
        botonEliminar = (Button) findViewById(R.id.btnEliminar);
        botonListar = (Button) findViewById(R.id.btnListar);
        botonEliminartodos = (Button) findViewById(R.id.btnEliminartodos);
        botonListartodos = (Button) findViewById(R.id.btnListartodos);
        datos = (TextView) findViewById(R.id.lblDatos);

        botonAgregar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonEliminartodos.setOnClickListener(this);
        botonListartodos.setOnClickListener(this);


        personabd = new HelperBD(this, "bda", null, 1);
    }
        @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAgregar:
                if(cajaCedula.getText().toString().equals("")||cajaNombres.getText().toString().equals("")||cajaApellidos.getText().toString().equals("")){
                    Toast toastt = Toast.makeText(getApplicationContext(), "NO DEJE CAMPOS EN BLANCO", Toast.LENGTH_SHORT);
                    cajaNombres.requestFocus();
                    toastt.show();

                }else {
                    personabd.insertar(cajaCedula.getText().toString(), cajaApellidos.getText().toString(), cajaNombres.getText().toString());
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    cajaNombres.setText("");
                    cajaNombres.requestFocus();
                    datos.setText("");
                    Toast toast = Toast.makeText(getApplicationContext(), "SE REGISTRO LA PERSONA", Toast.LENGTH_SHORT);
                    toast.show();
                }

                break;
            case R.id.btnModificar:

                if (cajaCedula.getText().toString().equals("")||cajaNombres.getText().toString().equals("")||cajaApellidos.getText().toString().equals("")){
                    Toast toast11 = Toast.makeText(getApplicationContext(), "POR FAVOR LLENE LOS DATOS PARA PODER MODIFICAR", Toast.LENGTH_SHORT);
                    cajaNombres.requestFocus();
                    toast11.show();

                }else {

                    personabd.modificar(cajaCedula.getText().toString(), cajaApellidos.getText().toString(), cajaNombres.getText().toString());

                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    cajaNombres.setText("");
                    cajaNombres.requestFocus();
                    datos.setText("");
                    Toast toast5 = Toast.makeText(getApplicationContext(), "SE MODIFICO LA PERSONA", Toast.LENGTH_SHORT);
                    toast5.show();

                }
                break;
            case R.id.btnEliminar:
                if (cajaCedula.getText().toString().equals("")){
                    Toast toast111 = Toast.makeText(getApplicationContext(), "POR FAVOR ESCRIBA LA CEDULA DE LA PERSONA QUE DESEA ELIMINAR ", Toast.LENGTH_SHORT);
                    cajaCedula.requestFocus();
                    toast111.show();
                }else {
                    personabd.eliminarcedula(cajaCedula.getText().toString());
                    Toast toast1 = Toast.makeText(getApplicationContext(), "PERSONA ELIMINADA \nLISTE PARA COMPROBAR", Toast.LENGTH_SHORT);
                    toast1.show();
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    cajaNombres.setText("");
                    cajaNombres.requestFocus();
                    datos.setText("");
                }
                break;
            case R.id.btnListar:

                if (cajaCedula.getText().toString().equals("")) {
                    Toast toast11111 = Toast.makeText(getApplicationContext(), "POR FAVOR ESCRIBA LA CEDULA DE LA PERSONA QUE DESEA ELIMINAR ", Toast.LENGTH_SHORT);
                    toast11111.show();
                    cajaCedula.requestFocus();
                }else {

                    datos.setText(personabd.leercedula(cajaCedula.getText().toString()));
                    Toast toast2 = Toast.makeText(getApplicationContext(), "SE LISTO LA PERSONA", Toast.LENGTH_SHORT);
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    cajaNombres.setText("");
                    cajaNombres.requestFocus();
                    toast2.show();

                }
                break;
            case R.id.btnEliminartodos:
                personabd.eliminar();
                Toast toast4 = Toast.makeText(getApplicationContext(), "SE ELIMINARON TODOS LOS REGISTRO" + cajaCedula.getText().toString(), Toast.LENGTH_SHORT);
                toast4.show();
                cajaApellidos.setText("");
                cajaCedula.setText("");
                cajaNombres.setText("");
                cajaNombres.requestFocus();
                datos.setText("");
                break;
            case R.id.btnListartodos:
                datos.setText(personabd.leer());
                cajaApellidos.setText("");
                cajaCedula.setText("");
                cajaNombres.setText("");
                cajaNombres.requestFocus();
                Toast toast3 = Toast.makeText(getApplicationContext(), "SE LISTARON TODOS LOS REGISTROS" + cajaCedula.getText().toString(), Toast.LENGTH_SHORT);
                toast3.show();
                break;

        }
    }
}